package MapModels;

import javafx.beans.property.SimpleDoubleProperty;

public class AzimuthModel {

    public double getLongitude1() {
        return longitude1.get();
    }

    public SimpleDoubleProperty longitude1Property() {
        return longitude1;
    }

    public void setLongitude1(double longitude1) {
        this.longitude1.set(longitude1);
    }

    public double getLatitude1() {
        return latitude1.get();
    }

    public SimpleDoubleProperty latitude1Property() {
        return latitude1;
    }

    public void setLatitude1(double latitude1) {
        this.latitude1.set(latitude1);
    }

    public double getLongitude2() {
        return longitude2.get();
    }

    public SimpleDoubleProperty longitude2Property() {
        return longitude2;
    }

    public void setLongitude2(double longitude2) {
        this.longitude2.set(longitude2);
    }

    public double getLatitude2() {
        return latitude2.get();
    }

    public SimpleDoubleProperty latitude2Property() {
        return latitude2;
    }

    public void setLatitude2(double latitude2) {
        this.latitude2.set(latitude2);
    }

    private SimpleDoubleProperty longitude1 = new SimpleDoubleProperty() ;
    private SimpleDoubleProperty latitude1 = new SimpleDoubleProperty() ;
    private SimpleDoubleProperty longitude2 = new SimpleDoubleProperty() ;
    private SimpleDoubleProperty latitude2 = new SimpleDoubleProperty() ;
    private SimpleDoubleProperty azimuth = new SimpleDoubleProperty();

    public double getAzimuth() {
        return azimuth.get();
    }

    public SimpleDoubleProperty azimuthProperty() {
        return azimuth;
    }

    public void setAzimuth(double azimuth) {
        this.azimuth.set(azimuth);
    }

    public AzimuthModel(){

    }

    public AzimuthModel(double longitude1, double latitude1, double longitude2, double latitude2, double azimuth){
        this.longitude1 = new SimpleDoubleProperty(longitude1);
        this.latitude1 = new SimpleDoubleProperty(latitude1);
        this.longitude2 = new SimpleDoubleProperty(longitude2);
        this.latitude2 = new SimpleDoubleProperty(latitude2);
        this.azimuth = new SimpleDoubleProperty(azimuth);
    }

}
