package MapModels;

import com.esri.arcgisruntime.mapping.view.Graphic;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.image.ImageView;

public class MyStationsModel {

    public MyStationsModel(ImageView image, SimpleDoubleProperty latitude, SimpleDoubleProperty longitude, SimpleStringProperty signature, SimpleStringProperty code) {
        this.image = image;
        this.latitude = latitude;
        this.longitude = longitude;
        this.signature = signature;
        this.code = code;
    }

    public MyStationsModel() {
    }


    public ImageView getImage() {
        return image;
    }

    public void setImage(ImageView image) {
        this.image = image;
    }

    public double getLatitude() {
        return latitude.get();
    }

    public SimpleDoubleProperty latitudeProperty() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude.set(latitude);
    }

    public double getLongitude() {
        return longitude.get();
    }

    public SimpleDoubleProperty longitudeProperty() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude.set(longitude);
    }

    public String getSignature() {
        return signature.get();
    }

    public SimpleStringProperty signatureProperty() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature.set(signature);
    }

    public Graphic getMyStationModelImage() {
        return myStationModelImage;
    }

    public void setMyStationModelImage(Graphic myStationModelImage) {
        this.myStationModelImage = myStationModelImage;
    }

    public String getCode() {
        return code.get();
    }

    public SimpleStringProperty codeProperty() {
        return code;
    }

    public void setCode(String code) {
        this.code.set(code);
    }

    private ImageView image = new ImageView();
    private SimpleDoubleProperty latitude = new SimpleDoubleProperty();
    private SimpleDoubleProperty longitude = new SimpleDoubleProperty();
    private SimpleStringProperty signature = new SimpleStringProperty();
    private Graphic myStationModelImage = new Graphic();
    private SimpleStringProperty code = new SimpleStringProperty();
}
