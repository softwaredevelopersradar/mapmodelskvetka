package MapModels;

import javafx.beans.property.SimpleDoubleProperty;

public class EDPOVModel {


    public double getRadiopodavlDist() {
        return radiopodavlDist.get();
    }

    public SimpleDoubleProperty radiopodavlDistProperty() {
        return radiopodavlDist;
    }

    public void setRadiopodavlDist(double radiopodavlDist) {
        this.radiopodavlDist.set(radiopodavlDist);
    }


    public double getMaxSvyazDist() {
        return maxSvyazDist.get();
    }

    public SimpleDoubleProperty maxSvyazDistProperty() {
        return maxSvyazDist;
    }

    public void setMaxSvyazDist(double maxSvyazDist) {
        this.maxSvyazDist.set(maxSvyazDist);
    }


    public double getPeredatchKompleksaPower() {
        return peredatchKompleksaPower.get();
    }

    public SimpleDoubleProperty peredatchKompleksaPowerProperty() {
        return peredatchKompleksaPower;
    }

    public void setPeredatchKompleksaPower(double peredatchKompleksaPower) {
        this.peredatchKompleksaPower.set(peredatchKompleksaPower);
    }


    public double getKoefUsilPeredAntennaKompleksa() {
        return koefUsilPeredAntennaKompleksa.get();
    }

    public SimpleDoubleProperty koefUsilPeredAntennaKompleksaProperty() {
        return koefUsilPeredAntennaKompleksa;
    }

    public void setKoefUsilPeredAntennaKompleksa(double koefUsilPeredAntennaKompleksa) {
        this.koefUsilPeredAntennaKompleksa.set(koefUsilPeredAntennaKompleksa);
    }


    public double getKoefPolyariz() {
        return koefPolyariz.get();
    }

    public SimpleDoubleProperty koefPolyarizProperty() {
        return koefPolyariz;
    }

    public void setKoefPolyariz(double koefPolyariz) {
        this.koefPolyariz.set(koefPolyariz);
    }


    public double getPeredatchikProtivnPower() {
        return peredatchikProtivnPower.get();
    }

    public SimpleDoubleProperty peredatchikProtivnPowerProperty() {
        return peredatchikProtivnPower;
    }

    public void setPeredatchikProtivnPower(double peredatchikProtivnPower) {
        this.peredatchikProtivnPower.set(peredatchikProtivnPower);
    }


    public double getKoefUsilPeredAntennaProtivn() {
        return koefUsilPeredAntennaProtivn.get();
    }

    public SimpleDoubleProperty koefUsilPeredAntennaProtivnProperty() {
        return koefUsilPeredAntennaProtivn;
    }

    public void setKoefUsilPeredAntennaProtivn(double koefUsilPeredAntennaProtivn) {
        this.koefUsilPeredAntennaProtivn.set(koefUsilPeredAntennaProtivn);
    }


    public double getKoefUsilPriemAntennaProtivn() {
        return koefUsilPriemAntennaProtivn.get();
    }

    public SimpleDoubleProperty koefUsilPriemAntennaProtivnProperty() {
        return koefUsilPriemAntennaProtivn;
    }

    public void setKoefUsilPriemAntennaProtivn(double koefUsilPriemAntennaProtivn) {
        this.koefUsilPriemAntennaProtivn.set(koefUsilPriemAntennaProtivn);
    }


    public double getKoefPodavl() {
        return koefPodavl.get();
    }

    public SimpleDoubleProperty koefPodavlProperty() {
        return koefPodavl;
    }

    public void setKoefPodavl(double koefPodavl) {
        this.koefPodavl.set(koefPodavl);
    }

    public double getKoefUsilPriemAntennaKompleksa() {
        return koefUsilPriemAntennaKompleksa.get();
    }

    public SimpleDoubleProperty koefUsilPriemAntennaKompleksaProperty() {
        return koefUsilPriemAntennaKompleksa;
    }

    public void setKoefUsilPriemAntennaKompleksa(double koefUsilPriemAntennaKompleksa) {
        this.koefUsilPriemAntennaKompleksa.set(koefUsilPriemAntennaKompleksa);
    }

    public double getSbrBisectrix() {
        return sbrBisectrix.get();
    }

    public SimpleDoubleProperty sbrBisectrixProperty() {
        return sbrBisectrix;
    }

    public void setSbrBisectrix(double sbrBisectrix) {
        this.sbrBisectrix.set(sbrBisectrix);
    }

    public double getSbrAngle() {
        return sbrAngle.get();
    }

    public SimpleDoubleProperty sbrAngleProperty() {
        return sbrAngle;
    }

    public void setSbrAngle(double sbrAngle) {
        this.sbrAngle.set(sbrAngle);
    }

    public EDPOVModel() {

    }

    private SimpleDoubleProperty radiopodavlDist = new SimpleDoubleProperty();
    private SimpleDoubleProperty maxSvyazDist = new SimpleDoubleProperty();
    private SimpleDoubleProperty peredatchKompleksaPower = new SimpleDoubleProperty();
    private SimpleDoubleProperty koefUsilPeredAntennaKompleksa = new SimpleDoubleProperty();
    private SimpleDoubleProperty koefUsilPriemAntennaKompleksa = new SimpleDoubleProperty();
    private SimpleDoubleProperty koefPolyariz = new SimpleDoubleProperty();
    private SimpleDoubleProperty peredatchikProtivnPower = new SimpleDoubleProperty();
    private SimpleDoubleProperty koefUsilPeredAntennaProtivn = new SimpleDoubleProperty();
    private SimpleDoubleProperty koefUsilPriemAntennaProtivn = new SimpleDoubleProperty();
    private SimpleDoubleProperty koefPodavl = new SimpleDoubleProperty();
    private SimpleDoubleProperty sbrBisectrix = new SimpleDoubleProperty();
    private SimpleDoubleProperty sbrAngle = new SimpleDoubleProperty();


    public EDPOVModel(double maxSvyazDist, double peredatchKompleksaPower, double koefUsilPeredAntennaKompleksa, double koefUsilPriemAntennaKompleksa, double koefPolyariz, double peredatchikProtivnPower, double koefUsilPeredAntennaProtivn, double koefUsilPriemAntennaProtivn, double koefPodavl, double sbrBisectrix, double sbrAngle) {
        this.maxSvyazDist = new SimpleDoubleProperty(maxSvyazDist);
        this.peredatchKompleksaPower = new SimpleDoubleProperty(peredatchKompleksaPower);
        this.koefUsilPeredAntennaKompleksa = new SimpleDoubleProperty(koefUsilPeredAntennaKompleksa);
        this.koefUsilPriemAntennaKompleksa = new SimpleDoubleProperty(koefUsilPriemAntennaKompleksa);
        this.koefPolyariz = new SimpleDoubleProperty(koefPolyariz);
        this.peredatchikProtivnPower = new SimpleDoubleProperty(peredatchikProtivnPower);
        this.koefUsilPeredAntennaProtivn = new SimpleDoubleProperty(koefUsilPeredAntennaProtivn);
        this.koefUsilPriemAntennaProtivn = new SimpleDoubleProperty(koefUsilPriemAntennaProtivn);
        this.koefPodavl = new SimpleDoubleProperty(koefPodavl);
        this.sbrBisectrix = new SimpleDoubleProperty(sbrBisectrix);
        this.sbrAngle = new SimpleDoubleProperty(sbrAngle);
    }
}
