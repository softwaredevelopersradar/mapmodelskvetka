package MapModels;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class EDPRVModel {


    public double getIonosphHeightMax() {
        return ionosphHeightMax.get();
    }

    public SimpleDoubleProperty ionosphHeightMaxProperty() {
        return ionosphHeightMax;
    }

    public void setIonosphHeightMax(double ionosphHeightMax) {
        this.ionosphHeightMax.set(ionosphHeightMax);
    }

    public double getMinSvyazDist() {
        return minSvyazDist.get();
    }

    public SimpleDoubleProperty minSvyazDistProperty() {
        return minSvyazDist;
    }

    public void setMinSvyazDist(double minSvyazDist) {
        this.minSvyazDist.set(minSvyazDist);
    }

    public double getRadiorazvedDist() {
        return radiorazvedDist.get();
    }

    public SimpleDoubleProperty radiorazvedDistProperty() {
        return radiorazvedDist;
    }

    public void setRadiorazvedDist(double radiorazvedDist) {
        this.radiorazvedDist.set(radiorazvedDist);
    }

    public double getPeredatchPomechPower() {
        return peredatchPomechPower.get();
    }

    public SimpleDoubleProperty peredatchPomechPowerProperty() {
        return peredatchPomechPower;
    }

    public void setPeredatchPomechPower(double peredatchPomechPower) {
        this.peredatchPomechPower.set(peredatchPomechPower);
    }

    public double getKoefUsilAntennaKompleksa() {
        return koefUsilAntennaKompleksa.get();
    }

    public SimpleDoubleProperty koefUsilAntennaKompleksaProperty() {
        return koefUsilAntennaKompleksa;
    }

    public void setKoefUsilAntennaKompleksa(double koefUsilAntennaKompleksa) {
        this.koefUsilAntennaKompleksa.set(koefUsilAntennaKompleksa);
    }

    public double getRassoglasKoef() {
        return rassoglasKoef.get();
    }

    public SimpleDoubleProperty rassoglasKoefProperty() {
        return rassoglasKoef;
    }

    public void setRassoglasKoef(double rassoglasKoef) {
        this.rassoglasKoef.set(rassoglasKoef);
    }

    public double getPeredatchikPower() {
        return peredatchikPower.get();
    }

    public SimpleDoubleProperty peredatchikPowerProperty() {
        return peredatchikPower;
    }

    public void setPeredatchikPower(double peredatchikPower) {
        this.peredatchikPower.set(peredatchikPower);
    }

    public double getKoefUsilAntennaRadiost() {
        return koefUsilAntennaRadiost.get();
    }

    public SimpleDoubleProperty koefUsilAntennaRadiostProperty() {
        return koefUsilAntennaRadiost;
    }

    public void setKoefUsilAntennaRadiost(double koefUsilAntennaRadiost) {
        this.koefUsilAntennaRadiost.set(koefUsilAntennaRadiost);
    }

    public double getKoefPodavl() {
        return koefPodavl.get();
    }

    public SimpleDoubleProperty koefPodavlProperty() {
        return koefPodavl;
    }

    public void setKoefPodavl(double koefPodavl) {
        this.koefPodavl.set(koefPodavl);
    }

    public double getDistRadiopodavl() {
        return distRadiopodavl.get();
    }

    public SimpleDoubleProperty distRadiopodavlProperty() {
        return distRadiopodavl;
    }

    public void setDistRadiopodavl(double distRadiopodavl) {
        this.distRadiopodavl.set(distRadiopodavl);
    }

    public int getProtivnikQuantity() {
        return protivnikQuantity.get();
    }

    public SimpleIntegerProperty protivnikQuantityProperty() {
        return protivnikQuantity;
    }

    public void setProtivnikQuantity(int protivnikQuantity) {
        this.protivnikQuantity.set(protivnikQuantity);
    }

    public double getSbrBisectrix() {
        return sbrBisectrix.get();
    }

    public SimpleDoubleProperty sbrBisectrixProperty() {
        return sbrBisectrix;
    }

    public void setSbrBisectrix(double sbrBisectrix) {
        this.sbrBisectrix.set(sbrBisectrix);
    }

    public double getSbrAngle() {
        return sbrAngle.get();
    }

    public SimpleDoubleProperty sbrAngleProperty() {
        return sbrAngle;
    }

    public void setSbrAngle(double sbrAngle) {
        this.sbrAngle.set(sbrAngle);
    }

    private SimpleDoubleProperty ionosphHeightMax = new SimpleDoubleProperty() ;
    private SimpleDoubleProperty minSvyazDist = new SimpleDoubleProperty() ;
    private SimpleDoubleProperty radiorazvedDist = new SimpleDoubleProperty() ;
    private SimpleDoubleProperty peredatchPomechPower = new SimpleDoubleProperty() ;
    private SimpleDoubleProperty koefUsilAntennaKompleksa = new SimpleDoubleProperty() ;
    private SimpleDoubleProperty rassoglasKoef = new SimpleDoubleProperty() ;
    private SimpleDoubleProperty peredatchikPower = new SimpleDoubleProperty() ;
    private SimpleDoubleProperty koefUsilAntennaRadiost = new SimpleDoubleProperty() ;
    private SimpleDoubleProperty koefPodavl = new SimpleDoubleProperty() ;
    private SimpleDoubleProperty distRadiopodavl = new SimpleDoubleProperty() ;
    private SimpleIntegerProperty protivnikQuantity= new SimpleIntegerProperty() ;
    ///для отрисовки сектора СБР
    private SimpleDoubleProperty sbrBisectrix = new SimpleDoubleProperty();
    private SimpleDoubleProperty sbrAngle = new SimpleDoubleProperty();

    public EDPRVModel(){

    }

    public EDPRVModel(double ionosphHeightMax, double peredatchPomechPower, double koefUsilAntennaKompleksa, double rassoglasKoef, double peredatchikPower, double koefUsilAntennaRadiost, double koefPodavl, double sbrBisectrix, double sbrAngle, int protivnikQuantity){
        this.ionosphHeightMax = new SimpleDoubleProperty(ionosphHeightMax);
        this.peredatchPomechPower = new SimpleDoubleProperty(peredatchPomechPower);
        this.koefUsilAntennaKompleksa = new SimpleDoubleProperty(koefUsilAntennaKompleksa);
        this.rassoglasKoef = new SimpleDoubleProperty(rassoglasKoef);
        this.peredatchikPower = new SimpleDoubleProperty(peredatchikPower);
        this.koefUsilAntennaRadiost = new SimpleDoubleProperty(koefUsilAntennaRadiost);
        this.koefPodavl = new SimpleDoubleProperty(koefPodavl);
        this.protivnikQuantity = new SimpleIntegerProperty(protivnikQuantity);
        this.sbrBisectrix = new SimpleDoubleProperty(sbrBisectrix);
        this.sbrAngle = new SimpleDoubleProperty(sbrAngle);
    }



}
