package MapModels;

import javafx.beans.property.SimpleDoubleProperty;

public class RouteModel {
    public RouteModel() {
    }

    public double getLatitude() {
        return latitude.get();
    }

    public SimpleDoubleProperty latitudeProperty() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude.set(latitude);
    }

    public double getLongitude() {
        return longitude.get();
    }

    public SimpleDoubleProperty longitudeProperty() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude.set(longitude);
    }

    public RouteModel(SimpleDoubleProperty latitude, SimpleDoubleProperty longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    private SimpleDoubleProperty latitude = new SimpleDoubleProperty();
    private SimpleDoubleProperty longitude = new SimpleDoubleProperty();
}
